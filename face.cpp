#include <dlib/opencv.h>
#include <opencv2/opencv.hpp>
#include <dlib/image_processing/frontal_face_detector.h>
#include <dlib/image_processing/render_face_detections.h>
#include <dlib/image_processing.h>
#include <dlib/gui_widgets.h>
#include <iostream>
#include<string>
#include <vector>
#include <cmath>
#include <time.h>
#include <vector>
using namespace std;
using namespace dlib;
using namespace cv;
#include <chrono> // for std::chrono functions

class Timer
{
private:
    // Type aliases to make accessing nested type easier
    using clock_t = std::chrono::steady_clock;
    using second_t = std::chrono::duration<double, std::ratio<1>>;

    std::chrono::time_point<clock_t> m_beg;

public:
    Timer() : m_beg(clock_t::now())
    {
    }

    void reset()
    {
        m_beg = clock_t::now();
    }

    double elapsed() const
    {
        return std::chrono::duration_cast<second_t>(clock_t::now() - m_beg).count();
    }
};

int main(int argc, char **argv)
{
    Timer t;
    //Draw axis
    Mat Eye_Waveform = Mat::zeros(900, 900, CV_8UC3); // Waveform image used to record blinks
    Point p1 = Point(10, 0);
    Point p2 = Point(10, 900);
    Point p3 = Point(0, 890);
    Point p4 = Point(900, 890);
    Scalar line_color = Scalar(255, 255, 255);
    cv::line(Eye_Waveform, p1, p2, line_color, 1, LINE_AA);
    cv::line(Eye_Waveform, p3, p4, line_color, 1, LINE_AA);

    int eye_previous_x = 10;  //The abscissa of the origin
    int eye_previous_y = 890; //The ordinate of the origin
    int eye_now_x = 1;
    int eye_now_y = 1;

    unsigned int count_blink = 0; //Number of blinks

    float blink_EAR_before = 0.0; // before blinking
    float blink_EAR_now = 0.2;    //In blinking
    float blink_EAR_after = 0.0;  //After blinking

    frontal_face_detector detector = get_frontal_face_detector();
    shape_predictor pos_modle;

    deserialize("shape_predictor_68_face_landmarks.dat") >> pos_modle;
    try
    {
        while (argc)
        {
            Mat temp;
            temp = imread(argv[i]);
            //cap >> temp;

            //Convert the image into the form of BGR in dlib
            cv_image<bgr_pixel> cimg(temp);

            std::vector<dlib::rectangle> faces = detector(cimg);
            std::vector<full_object_detection> shapes;
            unsigned int faceNumber = faces.size(); //Get the number of vectors in the container, that is, the number of faces
            for (unsigned int i = 0; i < faceNumber; i++)
            {
                shapes.push_back(pos_modle(cimg, faces[i]));
            }
            if (!shapes.empty())
            {
                //left
                unsigned int x_37 = shapes[0].part(37).x();
                unsigned int y_37 = shapes[0].part(37).y();

                unsigned int x_34 = shapes[0].part(34).x();
                unsigned int y_34 = shapes[0].part(34).y();

                //right
                unsigned int x_46 = shapes[0].part(46).x();
                unsigned int y_46 = shapes[0].part(46).y();

                //The coordinates of point 41
                unsigned int x_49 = shapes[0].part(49).x();
                unsigned int y_49 = shapes[0].part(49).y();

                unsigned int x_63 = shapes[0].part(63).x();
                unsigned int y_63 = shapes[0].part(63).y();

                unsigned int x_9 = shapes[0].part(9).x();
                unsigned int y_9 = shapes[0].part(9).y();

                unsigned int l1 = sqrt((x_34 - x_37) * (x_34 - x_37) + (y_34 - y_37) * (y_34 - y_37));
                unsigned int l2 = sqrt((x_34 - x_46) * (x_34 - x_46) + (y_34 - y_46) * (y_34 - y_46));

                //unsigned int l3 = sqrt((x_63 - x_37) * (x_63 - x_37) + (y_63 - y_37) * (y_63 - y_37));
                //unsigned int l4 = sqrt((x_63 - x_46) * (x_63 - x_46) + (y_63 - y_46) * (y_63 - y_46));

                //unsigned int l5 = sqrt((x_9 - x_37) * (x_9 - x_37) + (y_9 - y_37) * (y_9 - y_37));
                //unsigned int l6 = sqrt((x_9 - x_46) * (x_9 - x_46) + (y_9 - y_46) * (y_9 - y_46));

                //cout << "l-left with 9 = " << l5<<endl;
                //cout<<"l-right with 9 = "<<l6<<endl;
                //cout << "l-left with 63 = " << l3<<endl;
                //cout<<"l-right with 63 = "<<l4<<endl;
                //cout << "l-left with 34 = " << l1 << endl;
                //cout << "l-right with 34 = " << l2 << endl;
                cout<<"ratio = "<<(l1+0.0)/l2*1.0<<endl;

            }
            cv::imshow("Dlib tag", temp);
            waitKey(0);
            cin.get();
            //std::cout << "Time taken: " << t.elapsed() << " seconds\n";
        }
    }
    catch (serialization_error &e)
    {
        cout << "You need dlib‘s default face landmarking file to run this example." << endl;
        cout << endl
             << e.what() << endl;
    }
    catch (exception &e)
    {
        cout << e.what() << endl;
    }
}